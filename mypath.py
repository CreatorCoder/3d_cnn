from os.path import abspath, dirname, join


class Path(object):
    @staticmethod
    def db_dir(database):

        if database == 'ucf101':
            # folder that contains class labels
            #root_dir = abspath('/Users/dmitrii/Documents/НЕТОЛОГИЯ КУРС DLL/NeuroLab/Data/UCF101/data')  #'/Path/to/UCF-101'

            root_dir = abspath(r'D:\REPOSITORIES\NetologiaEngine\Data\UCF101\data')

            # Save preprocess data into output_dir
            output_dir = abspath(join(dirname(__file__), './Output')) #'/path/to/VAR/ucf101'

            return root_dir, output_dir

        elif database == 'hmdb51':
            # folder that contains class labels
            root_dir = '/Path/to/hmdb-51'

            output_dir = '/path/to/VAR/hmdb51'

            return root_dir, output_dir
        else:
            print('Database {} not available.'.format(database))
            raise NotImplementedError

    @staticmethod
    def model_dir():
        return abspath(join(dirname(__file__), './models/c3d-pretrained.pth'))   #'/path/to/Models/c3d-pretrained.pth'
