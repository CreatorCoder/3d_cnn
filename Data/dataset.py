import pickle
from os.path import abspath, dirname, join
import torch
from PIL import Image
from torch.utils.data import Dataset
import glob
import matplotlib.pyplot as plt
from torchvision.transforms import transforms


transform_frame_ucf101 = transforms.Compose([
    transforms.ToPILImage(),
    #transforms.Resize(299),
    transforms.Resize((171, 128)),
    #transforms.CenterCrop(112),
    transforms.ToTensor(),
    #transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]),
])


def transform_ucf101_for_inception3(video_clip):
    """
    Трансформация видеороликов
    :param video_clip:
    :return: N x C x W x H
    """
    video_clip = video_clip / 255.0
    video_clip = video_clip.permute(0, 3, 1, 2)  # новый порядок измерений
    list_of_numpy = []

    for frame in video_clip:
        #print('frame in: {}'.format(frame.shape))
        frame = transform_frame_ucf101(frame)
        #print('frame out: {}'.format(frame.shape))
        list_of_numpy.append(frame.numpy())

    video_clip = torch.tensor(list_of_numpy)

    return video_clip


@DeprecationWarning
class ActionRecognitionUCF101Dataset(Dataset):
    """
    Самописный датасет не используется, так как есть стандартный
    """

    def __init__(self, root_dir, transform=None, type='train'):
        """
        Args:
            root_dir (string): Directory with all the images.
            transform (callable, optional): Optional transform to be applied
                on a sample.
        """
        self.root_dir = root_dir
        self.transform = transform

        self.data = []

        self.class2index = self.load_classes(abspath(join(self.root_dir, 'train_test_list/classInd.txt')))
        self.index2class = {k: v for v, k in self.class2index.items()}

        for path_file in glob.glob('{0}/train_test_list/{1}list*.txt'.format(abspath(self.root_dir), type)):
            with open(path_file, 'r') as f:
                for line in f:
                    name_file, index_class = line.split(' ')
                    self.data.append((name_file, int(index_class.strip('\n'))))

    def load_classes(self, path):
        output = {}
        with open(path, 'r', encoding='utf8') as f:
            for line in f.readlines():
                v, k = line.split(' ')
                output[k.strip('\n')] = int(v)
        return output

    def __len__(self):
        return len(self.data)

    def __getitem__(self, index):

        if torch.is_tensor(indexes):
            indexes = indexes.tolist()

        if not isinstance(indexes, list):
            indexes = [indexes]

        samples = [sample for indx, sample in enumerate(self.data) if indx in indexes]

        for i in range(len(samples)):
            image_path, label = samples[i]
            samples[i] = (Image.open(image_path), label)

        if self.transform:
            for i in range(len(samples)):
                image, label = samples[i]
                samples[i] = (self.transform(image), label)

        return samples


def load_dataset(path):
    with open(path, 'rb') as f:
        action_recognition_dataset = pickle.load(f)
    return action_recognition_dataset


def save_dataset(path, action_recognition_dataset):
    with open(path, 'wb') as f:
        pickle.dump(action_recognition_dataset, f)


# Global resource
# path_to_object = join(dirname(abspath(__file__)), 'dataset_UCF101.dataset')
# action_recognition_dataset = load_dataset(path_to_object)


if __name__ == '__main__':

    #action_recognition_dataset = ActionRecognitionUCF101Dataset('./')

    # action_recognition_dataset = torchvision.datasets.UCF101(root='./data', annotation_path='./train_test_list',
    #                                                          frames_per_clip=15, step_between_clips=15,
    #                                                          transform=transform_ucf101_for_inception3)
    #
    # save_dataset('dataset_UCF101.dataset', action_recognition_dataset)

    action_recognition_dataset = load_dataset('dataset_UCF101.dataset')

    fig = plt.figure()

    for i in range(len(action_recognition_dataset)):
        frames, _, label = action_recognition_dataset[i]
        count_frames = 0
        for frame in frames:
            plt.imshow(frame.permute(1, 2, 0) .numpy())
            plt.show()
            count_frames += 1
            print('Shape: {}'.format(frame.shape))
        print('{}:{}'.format(count_frames, label))
        break
