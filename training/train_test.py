import torch
from torch import nn

from .metrics import accuracy


def train(epoch, model, train_dataloader, optimizer, criterion, scheduler, log_writer=None):
    model.train()

    if scheduler:
        scheduler.step()

    epoch_loss = 0.0
    epoch_acc = 0.0

    total_samples = len(train_dataloader.dataset)

    for batch_indx, (video_clips, labels) in enumerate(train_dataloader):

        optimizer.zero_grad()

        outputs = model(video_clips)

        probs = nn.Softmax(dim=1)(outputs)
        preds = torch.max(probs, 1)[1]

        loss = criterion(outputs, labels)

        loss.backward()
        optimizer.step()

        epoch_loss += loss.item() * video_clips.size(0)
        epoch_acc += torch.sum(preds == labels.data)

        epoch_loss = epoch_loss / total_samples
        epoch_acc = epoch_acc.double() / total_samples

        if batch_indx != 0 and batch_indx % 2 == 0:
            print('[{}/{}] loss: {:.3f}, accuracy: {:.3f}'.format(batch_indx * train_dataloader.batch_size,
                                                                  total_samples, epoch_loss, epoch_acc))
    else:
        print('[{}/{}] loss: {:.3f}, accuracy: {:.3f}'.format(batch_indx * train_dataloader.batch_size, total_samples,
                                                              epoch_loss, epoch_acc))

    log_writer.add_scalar('Train_loss_epoch', epoch_loss, epoch)
    log_writer.add_scalar('Train_acc_epoch', epoch_acc, epoch)

    return epoch_loss, epoch_acc


def test(epoch, model, test_dataloader, criterion, log_writer=None):
    model.eval()

    epoch_loss = 0.0
    epoch_acc = 0.0

    total_samples = len(test_dataloader.dataset)

    with torch.no_grad():

        for batch_indx, (video_clips, labels) in enumerate(test_dataloader):

            outputs = model(video_clips.permute(0, 2, 1, 3, 4))  # N x C x T x W x H

            probs = nn.Softmax(dim=1)(outputs)
            preds = torch.max(probs, 1)[1]

            loss = criterion(outputs, labels)

            epoch_loss += loss.item() * video_clips.size(0)
            epoch_acc += torch.sum(preds == labels.data)

            epoch_loss = epoch_loss / total_samples
            epoch_acc = epoch_acc.double() / total_samples

            if batch_indx != 0 and batch_indx % 2 == 0:
                print('[{}/{}] loss: {:.3f}, accuracy: {:.3f}'.format(batch_indx * test_dataloader.batch_size,
                                                                      total_samples, epoch_loss, epoch_acc))
        else:
            print('[{}/{}] loss: {:.3f}, accuracy: {:.3f}'.format(batch_indx * test_dataloader.batch_size, total_samples,
                                                                  epoch_loss, epoch_acc))
    log_writer.add_scalar('Test_loss_epoch', epoch_loss, epoch)
    log_writer.add_scalar('Test_acc_epoch', epoch_acc, epoch)

    return epoch_loss, epoch_acc

