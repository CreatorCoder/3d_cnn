import torch
from torch import nn, optim
from torch.utils.data import DataLoader
from torch.utils.tensorboard import SummaryWriter

from Data.data_train_test import get_train_test_datasets
from Data.dataset_origin import VideoDataset
import models.C3D_model as C3D_model
from models.C3D_model import C3D
from training.train_test import train, test

EPOCHS = 10


def custom_collate(batch):
    """
    Эта функция правит ошибку разныого размера тензоров у аудио дорожки
    :param batch:
    :return:
    """
    filtered_batch = []
    for video, _, label in batch:
        filtered_batch.append((video, label))
    return torch.utils.data.dataloader.default_collate(filtered_batch)


if __name__ == '__main__':

    train_dataset, test_dataset = get_train_test_datasets(size='big')

    print('Train dataset size: {}'.format(len(train_dataset.indices)))
    print('Test dataset size: {}'.format(len(test_dataset.indices)))

    train_dataloader = DataLoader(dataset=train_dataset, batch_size=16, num_workers=2, shuffle=True, drop_last=True,
                                  collate_fn=custom_collate)
    test_dataloader = DataLoader(dataset=test_dataset, batch_size=8, num_workers=1, shuffle=False, drop_last=True,
                                 collate_fn=custom_collate)

    model = C3D(num_classes=101, pretrained=True)

    lr = 0.003
    train_params = [{'params': C3D_model.get_1x_lr_params(model), 'lr': lr},
                    {'params': C3D_model.get_10x_lr_params(model), 'lr': lr * 10}]

    criterion = nn.CrossEntropyLoss()  # standard crossentropy loss for classification
    optimizer = optim.SGD(train_params, lr=lr, momentum=0.9, weight_decay=5e-4)
    scheduler = optim.lr_scheduler.StepLR(optimizer, step_size=10,
                                          gamma=0.1)  # the scheduler divides the lr by 10 every 10 epochs

    writer = SummaryWriter(log_dir='./runs')


    for epoch in range(EPOCHS):

        #train(epoch, model, train_dataloader, optimizer, criterion, scheduler, log_writer=writer)

        test(epoch, model, test_dataloader, criterion, log_writer=writer)

    writer.close()



